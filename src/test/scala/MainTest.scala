import org.scalatest.FunSuite
import org.scalatest._
import Matchers._

import seartipy.scalaprograms.imperative._

class MainSuite extends FunSuite {
  test("sayHello") {
    assert( Main.sayHello("foo") == "hello, foo" )
  }
}
