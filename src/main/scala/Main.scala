package seartipy.scalaprograms.imperative

object Main extends App {
  def sayHello(name : String) : String = s"hello, ${name}"
}
